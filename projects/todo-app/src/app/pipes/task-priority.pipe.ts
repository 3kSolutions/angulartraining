import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../models/task';
import { TaskPriority } from '../models/task-priority.enum';

@Pipe({
  name: 'taskPriority'
})
export class TaskPriorityPipe implements PipeTransform {

  transform(tasks: Task[], priority: TaskPriority | string): Task[] {
    if (priority === 'false') {
      return tasks;
    }

    const result: Task[] = [];

    for (const task of tasks) {
      if (task.priority === priority) {
        result.push(task);
      }
    }

    return result;
  }

}
