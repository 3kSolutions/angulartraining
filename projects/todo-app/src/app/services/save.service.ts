import { Injectable } from '@angular/core';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class SaveService {

  constructor() { }

  save(tasks: Task[]): void {
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  load() {
    const tasksString = localStorage.getItem('tasks');

    if (!tasksString) {
      return [];
    }
    const savedTasks = JSON.parse(tasksString) as [];
    return savedTasks.map((task: Task) => new Task(task.desc, task.priority, task.creationDate, task.completionDate));
  }
}
