export enum TaskPriority {
    WYSOKI = 'high',
    SREDNI = 'medium',
    NISKI = 'low'
}
