import { TaskPriority } from './task-priority.enum';

export class Task {
    constructor(public desc: string, public priority: TaskPriority, public creationDate?: Date, public completionDate?) {
        if (!this.creationDate) {
            this.creationDate = new Date();
        }
    }

    completed() {
        this.completionDate = new Date();
    }
}
