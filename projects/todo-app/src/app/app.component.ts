import { Component } from '@angular/core';
import { Task } from './models/task';
import { TaskPriority } from './models/task-priority.enum';
import { SaveService } from './services/save.service';

@Component({
  selector: 'todo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  taskList: Task[] = [];
  priorities: [string, string][] = [];

  constructor(private saveService: SaveService) {
    for (const priority in TaskPriority) {
      if (isNaN(priority as any)) {
        this.priorities.push([priority, TaskPriority[priority]]);
      }
    }

    this.taskList = this.saveService.load();
  }

  addNewTask(desc: string, prio: TaskPriority): void{
    this.taskList = [new Task(desc, prio), ...this.taskList];
    this.saveService.save(this.taskList);
  }

  taskCompleted() {
    this.saveService.save(this.taskList);
  }

}
