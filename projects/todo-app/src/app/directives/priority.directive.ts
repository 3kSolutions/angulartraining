import { Directive, Input, ElementRef } from '@angular/core';
import { TaskPriority } from '../models/task-priority.enum';

@Directive({
  selector: '[todoPriority]'
})
export class PriorityDirective {

  @Input('todoPriority')
  set priority(val: TaskPriority) {
    switch (val) {
      case TaskPriority.WYSOKI :
        this.el.nativeElement.style.backgroundColor = 'rgba(255, 0, 0, 0.4)';
        break;
      case TaskPriority.SREDNI :
        this.el.nativeElement.style.backgroundColor = 'rgba(240, 171, 0, 0.4)';
        break;
      case TaskPriority.NISKI :
        this.el.nativeElement.style.backgroundColor = 'rgba(0, 255, 0, 0.4)';
        break;
    }
  }

  constructor(private el: ElementRef) {
  }
}
