import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemComponent } from './components/item/item.component';
import { TaskPriorityPipe } from './pipes/task-priority.pipe';
import { PriorityDirective } from './directives/priority.directive';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    TaskPriorityPipe,
    PriorityDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
