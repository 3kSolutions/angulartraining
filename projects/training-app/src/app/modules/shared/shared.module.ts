import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloWorldComponent } from '../../components/hello-world/hello-world.component';
import { FindDirective } from '../../directives/find.directive';

@NgModule({
  declarations: [
    HelloWorldComponent,
    FindDirective],
  imports: [
    CommonModule
  ],
  exports: [
    HelloWorldComponent,
    FindDirective]
})
export class SharedModule { }
