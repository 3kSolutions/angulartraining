import { Component } from '@angular/core';
import { Student } from './models/student';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  greetedText = '';

  today = new Date();
  toggle: boolean = false;

  students: Student[] = [
    { wiek: 17, imie: 'Mateusz' },
    { wiek: 18, imie: 'Dagmara' },
    { wiek: 50, imie: 'Marcin' },
    { wiek: 75, imie: 'Paweł' },
    { wiek: 80, imie: 'Magda' }
  ];

  constructor(private router: Router) {}

  onGreeted(value: string) {
    this.greetedText = value;
  }

  get format(): string {
    return this.toggle ? 'fullDate' : 'dd/MM/yyyy';
  }

  changeFormat() {
    this.toggle = !this.toggle;
  }

  dodaj(imie: string, wiek: number) {
    this.students = [...this.students, {imie, wiek}];
  }

  navigate(message: string) {
    this.router.navigate(['strona2', message]);
  }
}
