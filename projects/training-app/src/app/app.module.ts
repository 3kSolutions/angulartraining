import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SharedModule } from './modules/shared/shared.module';
import { FibonacciPipe } from './pipes/fibonacci.pipe';
import { PelnoletniPurePipe } from './pipes/pelnoletni-pure.pipe';
import { NiepelnoletniImpurePipe } from './pipes/niepelnoletni-impure.pipe';
import { Page1Component } from './pages/page1/page1.component';
import { Page2Component } from './pages/page2/page2.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { Page3Component } from './pages/page3/page3.component';
import { SubPage1Component } from './pages/page3/subpages/sub-page1/sub-page1.component';
import { SubPage2Component } from './pages/page3/subpages/sub-page2/sub-page2.component';

@NgModule({
  declarations: [
    AppComponent,
    FibonacciPipe,
    PelnoletniPurePipe,
    NiepelnoletniImpurePipe,
    Page1Component,
    Page2Component,
    PageNotFoundComponent,
    Page3Component,
    SubPage1Component,
    SubPage2Component
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
