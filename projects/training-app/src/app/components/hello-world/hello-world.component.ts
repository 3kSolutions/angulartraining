import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '../../services/logger.service';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css'],
  providers: [LoggerService]
})
export class HelloWorldComponent implements OnInit {

  @Input('welcome-text')
  message: string;

  @Output()
  greeted: EventEmitter<string> = new EventEmitter<string>();

  constructor(private logger: LoggerService) {
    this.logger.log('HelloWorldComponent');
  }

  ngOnInit(): void {
  }

  submit(value: string) {
    this.greeted.emit(value);
  }

}
