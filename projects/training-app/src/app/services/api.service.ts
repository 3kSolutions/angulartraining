import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../models/student';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('https://angulartraining.free.beeceptor.com/api/students');
  }

  addStudent(student: Student): Observable<void> {
    return this.http.post<void>('https://angulartraining.free.beeceptor.com/api/student', student);
  }
}
