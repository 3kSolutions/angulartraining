import { Injectable } from '@angular/core';

let instanceNumber = 1;

@Injectable({
    providedIn: 'root',
   })
export class LoggerService {
  currentInstanceNumber: number;

  constructor() {
    this.currentInstanceNumber = instanceNumber++;
  }

  log(msg: any) {
    console.log(`${this.currentInstanceNumber} - ${msg}`);
  }
}
