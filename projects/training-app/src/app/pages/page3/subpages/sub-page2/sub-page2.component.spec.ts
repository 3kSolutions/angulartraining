import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPage2Component } from './sub-page2.component';
import { of } from 'rxjs';
import { ApiService } from '../../../../services/api.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

describe('SubPage2Component', () => {
  let component: SubPage2Component;
  let fixture: ComponentFixture<SubPage2Component>;

  beforeEach(async(() => {
    const apiServiceSpy = jasmine.createSpyObj('ApiService', ['getStudents']);
    apiServiceSpy.getStudents.and.callFake(() => of([]));

    TestBed.configureTestingModule({
      declarations: [ SubPage2Component ],
      providers: [
        { provide: ApiService, useFactory: () => apiServiceSpy }
      ],
      imports: [CommonModule, FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
