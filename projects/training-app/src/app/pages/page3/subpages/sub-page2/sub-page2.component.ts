import { Component, OnInit } from '@angular/core';
import { Student } from 'projects/training-app/src/app/models/student';
import { ApiService } from 'projects/training-app/src/app/services/api.service';

@Component({
  selector: 'app-sub-page2',
  templateUrl: './sub-page2.component.html',
  styleUrls: ['./sub-page2.component.css']
})
export class SubPage2Component implements OnInit {

  student: Student = new Student();
  students: Student[] = [];

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getStudents().subscribe((students) => {
      this.students = students;
    });
  }

  addStudent() {
    const student = { ...this.student };
    this.api.addStudent(student).subscribe(() => {
      this.students = [...this.students, student];
      this.student = new Student();
    });
  }

}
