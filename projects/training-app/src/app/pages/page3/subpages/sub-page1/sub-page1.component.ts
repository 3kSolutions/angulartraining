import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-sub-page1',
  templateUrl: './sub-page1.component.html',
  styleUrls: ['./sub-page1.component.css']
})
export class SubPage1Component implements OnInit, OnDestroy {

  count: number = 0;
  subscription: Subscription;

  constructor() { }

  ngOnInit(): void {
    this.subscription = timer(5000, 1500).pipe(map((val) => val % 5)).subscribe((val) => this.count += val);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
