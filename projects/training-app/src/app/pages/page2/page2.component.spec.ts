import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page2Component } from './page2.component';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

describe('Page2Component', () => {
  let component: Page2Component;
  let fixture: ComponentFixture<Page2Component>;

  beforeEach(async(() => {
    const paramMap = new Map<string, string>();
    paramMap.set('message', 'message');
    TestBed.configureTestingModule({
      declarations: [ Page2Component ],
      providers: [
        { provide: ActivatedRoute, useValue: { paramMap: of(paramMap) } }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
