import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page1Component } from './page1.component';
import { ActivatedRoute } from '@angular/router';

describe('Page1Component', () => {
  let component: Page1Component;
  let fixture: ComponentFixture<Page1Component>;

  beforeEach(async(() => {
    const queryParamMap = new Map<string, string>();
    queryParamMap.set('message', 'Katowice');
    TestBed.configureTestingModule({
      declarations: [ Page1Component ],
      providers: [
        { provide: ActivatedRoute, useValue: { snapshot: { queryParamMap } } }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
