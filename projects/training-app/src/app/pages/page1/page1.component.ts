import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {

  message: string;

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(5)]),
    surname: new FormControl('', [Validators.required, Validators.maxLength(30)]),
    address: new FormGroup({
      street: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required)
    })
  });

  get name() {
    return this.form.get('name');
  }

  get surname() {
    return this.form.get('surname');
  }

  get street() {
    return this.form.get('address').get('street');
  }

  get city() {
    return this.form.get('address').get('city');
  }

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.message = this.activatedRoute.snapshot.queryParamMap.get('message');
  }

  setCity() {
    this.form.patchValue({ address: { city: 'Katowice'} });
  }

}
