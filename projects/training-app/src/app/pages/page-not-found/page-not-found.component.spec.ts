import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { PageNotFoundComponent } from './page-not-found.component';
import { Router } from '@angular/router';

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;

  beforeEach(async(() => {
    const routerServiceSpy = jasmine.createSpyObj('Router', ['navigate']);
    routerServiceSpy.navigate.and.callFake(() => {});

    TestBed.configureTestingModule({
      declarations: [ PageNotFoundComponent ],
      providers: [ { provide: Router, useFactory: () => routerServiceSpy } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));
});
