import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  timeleft: number;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.timeleft = 5;
    this.countDown();
  }

  countDown() {
    if (this.timeleft === 0) {
      this.router.navigate(['/'], { replaceUrl: true });
    } else {
      this.timeleft--;
      setTimeout(this.countDown.bind(this), 1000);
    }
  }

}
