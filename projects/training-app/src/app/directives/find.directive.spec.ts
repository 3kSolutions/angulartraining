import { FindDirective } from './find.directive';
import { LoggerService } from '../services/logger.service';
import { ElementRef } from '@angular/core';

describe('FindDirective', () => {
  it('should create an instance', () => {
    const directive = new FindDirective(new ElementRef<any>({style: {}, innerText: '', }), new LoggerService());
    expect(directive).toBeTruthy();
  });
});
