import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { LoggerService } from '../services/logger.service';

@Directive({
  selector: '[appFind]'
})
export class FindDirective {

  @Input('appFind')
  text: string = '';

  constructor(private el: ElementRef,
              private logger: LoggerService) {
      this.logger.log('PokolorujDirective');
      this.el.nativeElement.style.border = '1px solid black';
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.el.nativeElement.innerText &&
        this.el.nativeElement.innerText.indexOf(this.text) !== -1) {
        this.el.nativeElement.style.backgroundColor = 'orange';
      }
  }

}
