import { Pipe, PipeTransform } from '@angular/core';
import { Student } from '../models/student';
let counter: number = 0;

@Pipe({
  name: 'niepelnoletniImpure',
  pure: false
})
export class NiepelnoletniImpurePipe implements PipeTransform {

  transform(students: Student[], ): Student[] {
    console.log(`niepelnoletniImpure - ${counter++}`);

    const results = [];

    if (!students) {
      return results;
    }

    for (const student of students) {
      if (student.wiek < 18) {
        results.push(student);
      }
    }

    return results;
  }

}
