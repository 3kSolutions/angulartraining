import { Pipe, PipeTransform } from '@angular/core';
const memo = {};

@Pipe({
  name: 'fibonacci'
})
export class FibonacciPipe implements PipeTransform {

  transform(value: number): number[] {
    const fibonacci: number[] = [];

    for (let i = 0; i < value; i++) {
      fibonacci.push(this.fibonacci(i));
    }

    return fibonacci;
  }

  fibonacci(curr: number): number {
    if (memo[curr]) {
      return memo[curr];
    }

    if (curr <= 1) {
      return 1;
    }

    return memo[curr] = this.fibonacci(curr - 1) + this.fibonacci(curr - 2);
  }

}
