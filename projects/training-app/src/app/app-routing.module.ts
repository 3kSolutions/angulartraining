import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page1Component } from './pages/page1/page1.component';
import { Page2Component } from './pages/page2/page2.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { Page3Component } from './pages/page3/page3.component';
import { SubPage1Component } from './pages/page3/subpages/sub-page1/sub-page1.component';
import { SubPage2Component } from './pages/page3/subpages/sub-page2/sub-page2.component';


const routes: Routes = [
  { path: '', component: Page1Component, pathMatch: 'full'},
  { path: 'strona1', component: Page1Component },
  { path: 'strona2', component: Page2Component },
  { path: 'strona2/:message', component: Page2Component },
  { path: 'strona3', component: Page3Component,
    children: [
      { path: 'podstrona1', component: SubPage1Component },
      { path: 'podstrona2', component: SubPage2Component },
    ]
  },
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
